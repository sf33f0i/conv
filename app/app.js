import Vue from 'nativescript-vue'
import Navigator from 'nativescript-vue-navigator'
import Home from './components/Home'
import App from './components/App.vue'
import {routes} from '/routes.js'
Vue.use(Navigator, { routes })
new Vue({
  render: h => h(App),
}).$start()
