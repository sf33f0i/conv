import Home from '@/components/Home.vue'
import Weight from '@/components/Weight.vue'
import Temperature from '@/components/Temperature.vue'
import Long from '@/components/Long.vue'
import Square from '@/components/Square.vue'
import Energy from '@/components/Energy.vue'

export const routes = {
  '/home': {
    component: Home,
  },
  '/weight': {
    component: Weight,
  },  
  '/temperature': {
    component: Temperature,
  },
  '/long': {
    component: Long,
  },
  '/square': {
    component: Square,
  },
  '/energy' : {
    component : Energy,
  },
}